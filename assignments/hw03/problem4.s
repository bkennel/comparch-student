    .set noreorder
    .data
	record: .space 8
    .text
    .globl main
    .ent main
main:
	#record *r = (record *) MALLOC(sizeof(record));
	addi $a0, $zero, 8	#size of record
	ori $v0, $0, 9		#syscall for malloc
	syscall			#address of record stored in $v0
	addi $s0, $v0, 0	#$s0 stores address
	
	#r->field0 = 100;
	addi $t0, $zero, 0	#address offset
	addi $t1, $zero, 100	#r->field0=100
	sw $t1, record($t0)	#store t1 in r->field0
	#r->field1 = -1;
	addi $t0, $t0, 4	#increase offset for next field
	addi $t1, $zero, -1	#r->field1=-1
	sw $t1, record($t0)	#store t1 in r->field1
        ori $v0, $0, 10     # exit
        syscall
    .end main
