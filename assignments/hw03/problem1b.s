.set noreorder
.data
    A: .space 32	# int A[8]
.text
.globl main
.ent main

# void print(int a) {
print:
    add $a0, $zero, $a0	# set $a0 to a[i]
    ori $v0, $0, 20	# configure print syscall
    syscall		# print hex
    jr $ra              # return to caller
    nop

# int main () {
main:

    addi $s0, $zero, 0;        # s0 holds 0
    addi $s1, $zero, 1;        # s1 holds 1
    addi $t0, $zero, 0         # t0 holds 0 - array index

    sw $s0, A($t0)          # A[0] = 0
    addi $t0, $t0, 4        # increment index by 4
    sw $s1, A($t0)          # A[1] = 1

    addi $s0, $zero, 2         # i = 2

    # for (i = 2; i < 8; i++) {
    loop:
    slt $t0, $s0, 8    	# t0 = i < 8
    beq $t0, $zero, end # if(!t0) goto end
    nop
    addi $s1, $s0, 0    # s1 = i
    sub $s2, $s0, 1     # s2 = i - 1
    sub $s3, $s0, 2     # s3 = i - 2
    mul $s1, $s1, 4     # adjust to array index i
    mul $s2, $s2, 4     # adjust to index i-1
    mul $s3, $s3, 4     # adjust to index i-2
    lw $t0, A($s2)      # load A[i-1] into t0
    lw $t1, A($s3)      # load A[i-2] into t1
    add $t0, $t0, $t1   # t0 = t0 + t1
    sw $t0, A($s1)      # A[i] = A[i-1] + A[i-2]
    add $a0, $zero, $t0    # store A[i] in a0
    jal print           # print(A[i])
    nop
    addi $s0, $s0, 1    # i = i + 1
    j loop		# jump to start of loop
    
    end:
    ori $v0, $0, 10     # exit
    syscall

.end main
