    .set noreorder
    .data
        A: .word 1, 25, 7, 9, -1        # char A[] = {1, 25, 7, 9, -1};
    .text
    .globl main
    .ent main
main:

    addi $s0, $zero, 0		# i = 0
    lw  $s1, A                  # current = A[0]
    addi $s2, $zero, 0          # max = 0

    loop:
        sgt $t0, $s1, $s3       # t0 = current > 0
        beq $t0, $zero, end        # if(!t0) goto end
        nop                     # beq $s1, -1, done nop
        sgt $t1, $s1, $s2       # t1 = current > max
        beq $t1, $zero, setmax     # if(!t1) goto setmax
        nop
        add $s2, $zero, $s1        # max = current

    setmax:
        addi $s0, $s0, 1        # i = i + 1
        mul $t2, $s0, 4         # t2 = i * 4 (align with next array index)
        lw $s1, A($t2)          # current = a[i]
        j loop                  # jump to loop
        nop

    end:
        add $a0, $zero, $s2     # set $a0 to max to print
    	ori $v0, $0, 20		# configure system call to print hex ($a0)
    	syscall		        # print hex
        ori $v0, $0, 10         # configure system call to exit
        syscall                 # exit
    .end main
