    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:
	addi $s0, $zero, 84	# int score=84
	slt $t0, $s0, 90      	# t0 = score < 90
	bne $t0, $zero, less80 	# if t0=0, branch to less80
	addi $s1, $zero, 4     	# grade = 4;
	j endif			# jump to endif

	less80:
	#else if (score >= 80)
	slt $t0, $s0, 80	# t0 = score < 80
	bne $t0, $zero, less70	# if t0=0, branch to less70
	addi $s1, $zero, 3	# grade=3;
	j endif		 	# jump to endif

	less70:
	#else if (score >= 70)
	slt $t0, $s0, 70	# t0 = score < 70
	bne $t0, $zero, else    # if(t0) goto else
	addi $s1, $zero, 2	# grade=2
	j endif			# jump to endif

	else:
	addi $s1, $zero, 0	# grade=0

	endif:
	#PRINT_HEX_DEC(grade);
        add $a0, $0, $s1	# store grade in a0 to pass to print syscall
	ori $v0, $0, 20		# configure system call to configure hex of grade ($a0)
	syscall			# print grade in hex
	#EXIT;
	ori $v0, $0, 10     	# configure system call to exit
        syscall		        # exit
    .end main

