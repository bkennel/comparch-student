    .set noreorder
    .data

    .text
    .globl main
    .ent main

#void print(int a)
print:
	ori $v0, $0, 20		#configure print syscall
	syscall
	jr $ra			#return to caller
	nop

#int sum3(int a, int b, int c)
sum3:
	#return a+b+c;
	add $t0, $a0, $a1	#t0=a+b
	add $t0, $t0, $a2	#t0=t0+c
	addi $v0, $t0, 0	#store t0 in v0
	jr $ra			#return to caller
	nop

#int polynomial(int a, int b, int c, int d, int e)
polynomial:
	addi $sp, $sp, -4	#add space on stack
	sw $ra, 0($sp)		#store ra on stack
	#load e from stack
	lw $s2, 4($sp)		#load e from stack
	#int x;
	#int y;
	#int z;
	sll $t0, $a0, $a1	#x = a << b;
	sll $t1, $a2, $a3	#y = c << d;
	#z = sum3(x, y, e);
	addi $a0, $t0, 0	#store x, y, and e in a registers to be
	addi $a1, $t1, 0	#passed as parameters to sum3
	addi $a2, $s2, 0
	jal sum3		#call sum3
	nop
	addi $s3, $v0, 0	#store return value of sum3 as z

	jal print		#print(x);
	nop
	addi $t2, $a0, 0	#store a0 in t register

	addi $a0, $a1, 0	#store y in a0 to pass to print
	jal print		#print(y);
	nop
	addi $t3, $a0, 0	#store a0 in t
	
	addi $a0, $s3, 0	#store z in a0
	jal print		#print(z);
	nop
	addi $t4, $a0, 0	#store a0 in t
	
	addi $v0, $t4, 0	#store z in v0 to return to caller
	#load return address
	lw $ra, 0($sp)		#load return address from stack
	addi $sp, $sp, 4	#reset stack pointer (pop)
	jr $ra			#return to caller
	nop

#int main()
main:
	addi $sp, $sp, -4	#add space on stack
	sw $ra, 0($sp)		#store return address on stack

	addi $s0, $zero, 2	#int a=2;
	addi $a0, $s0, 0	#pass a as parameter
	#int f = polynomial(a, 3, 4, 5, 6);
	addi $a1, $zero, 3	#store parameters in a registers to
	addi $a2, $zero, 4	#pass to function
	addi $a3, $zero, 5
	#store fifth parameter  on stack
	addi $t0, $zero, 6
	addi $sp, $sp, -4	#make stack space for parameter
	sw $t0, 0($sp)		#save parameter to stack
	jal polynomial		#call polynomial function
	nop
	addi $sp, $sp, 4 	#restore stack pointer - extra function parameter not needed
	addi $s1, $v0, 0	#store return value of polynomial
	addi $a0, $s0, 0	#store a in a0 to print
	jal print		#print(a);
	nop

	addi $a0, $s1, 0	#store return value of polynomial
	jal print		#print(f);
	nop			

	lw $ra, 0($sp)		#load return address from stack
	jr $ra			#return to caller
	nop
    .end main

