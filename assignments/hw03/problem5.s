    .set noreorder
    .data
	elt: .space 8
    .text
    .globl main
    .ent main
main:
#	elt *head;
#	elt *newelt;
#	newelt = (elt *) MALLOC(sizeof (elt));
	addi $a0, $zero, 8	#allocate 8 bytes for newelt
	ori $v0, $0, 9		#configure syscall for malloc
	syscall
	addi $s0, $v0, 0	#s0 stores address of new elt

	addi $t0, $zero, 1
	sw $t0, 0($s0)		#newelt->value=1;

	addi $t1, $zero, 0
	sw $t1, 4($s0)		#newelt->next=0;

#	head = newelt;
	addi $s1, $s0, 0	#s1 stores address of head
#	newelt = (elt *) MALLOC(sizeof (elt));
	addi $a0, $zero, 8	#allocate 8 bytes for new elt
	ori $v0, $0, 9		#configure malloc syscall
	syscall
	addi $s2, $v0, 0	#s2 stores address of newelt

	addi $t2, $zero, 2
	sw $t2, 0($s2)		#newelt->value=2;

#	newelt->next = head;
	sw $s1, 4($s2)		#stores head in newelt
#	head = newelt;
	addi $s1, $s2, 0
#	PRINT_HEX_DEC(head->value);
	lw $a0, 0($s1)		#load head->value to a0
	ori $v0, $0, 20		#configure print syscall
	syscall
#	PRINT_HEX_DEC(head->next->value);
	lw $t3, 4($s1)		#t3 stores next address
	lw $t4, 0($t3)		#load next->value from t3
	addi $a0, $t4, 0	#store value in a0
	ori $v0, $0, 20		#configure print syscall
	syscall

        ori $v0, $0, 10     # exit
        syscall
    .end main
