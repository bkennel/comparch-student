    .set noreorder
    .data
	array: .space 32
    .text
    .globl main
    .ent main
main:
	addi $t0, array
	addi $s0, $zero, 0
	addi $s1, $zero, 1

	sw $s0, 0($t0)
	sw $s1, 4($t0)

	sw $s0, array($t0)
	add $a0, $s0, $zero
		addi $t0, $t0, 4
	ori $v0, $0, 20
	syscall
	sw $s1, array($t0)
	
		addi $t0, $t0, 4
	add $a0, $s1, $zero
	ori $v0, $0, 20
	syscall

        ori $v0, $0, 10     # exit
        syscall
    .end main
